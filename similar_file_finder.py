from os import listdir, remove
from os.path import isfile, join

from sklearn.feature_extraction.text import TfidfVectorizer


def search_dict_for_term(dict_to_search, search_term, subkey='similar_to'):
    """
    Helper function to find search terms in a multi-level dictionary.
    :param dict_to_search: The dictionary to search.
    :param search_term: The term to search for.
    :param subkey: The dictionary subkey to compare to (default is `similar_to`).
    :return: True or False depending on whether search_term was located in dict_to_search.
    """
    for key in dict_to_search:
        if search_term == dict_to_search[key][subkey]:
            return True
    return False


class SimilarFileFinder:
    """
    Finds similar files in directory tree by transforming a corpus of text files into a TF-IDF vector
    and then comparing the cosine similarity for each file.
    """

    def __init__(self, data_path, similarity_threshold):
        """
        Instantiates a SimilarFileFinder object.
        :param data_path: The data path to search for similar files.
        :param similarity_threshold: Percentage value which specifies how similar files must be in order to be removed.
        """
        self.data_path = data_path
        self.files_to_delete = set()
        self.similarity_threshold = similarity_threshold / 100

    def delete_similar_files(self):
        """
        Deletes similar files by iterating over the files_to_delete set.
        """
        if len(self.files_to_delete) > 0:
            for file in self.files_to_delete:
                remove(file)
            print('Successfully deleted {} similar files:\n{}'.format(len(self.files_to_delete),
                                                                      [file for file in sorted(self.files_to_delete)]))
            self.files_to_delete = set()
        else:
            print('No similar files found')

    def find_similar_files(self):
        """
        Finds similar files by creating a TF-IDF vector from a corpus of files in the data path and
        then creating a similarity array where any element [i][j] should be equal to element [j][i],
        since cosine similarity is commutative.

        Iterating through the similarity array with a O(n^2) loop and skipping exact comparisons,
        the current file name, its similar counterpart, and their similarity is added to a dict of
        similar files if similarity between the two exceeds the similarity threshold.

        The search_dict_for_term() helper function is used to determine whether the current file
        being processed has previously been added as a similar file, in which case the file name
        is added to a set of files to delete.
        """
        file_list = ['{0}/{1}'.format(self.data_path, f) for f in listdir(self.data_path)
                     if isfile(join(self.data_path, f))]
        corpus = [open(f, 'r', encoding='utf-8', errors='ignore').read() for f in file_list]
        tfidf = TfidfVectorizer().fit_transform(corpus)
        pairwise_similarity_array = (tfidf * tfidf.T).A
        similar_files = {}

        for i in range(0, len(pairwise_similarity_array)):
            for j in range(0, len(pairwise_similarity_array[i])):
                current_file_name = file_list[i]
                similar_file_name = file_list[j]
                similarity = pairwise_similarity_array[i][j]

                # Skip comparisons to the exact same file
                if current_file_name == similar_file_name:
                    continue

                if similarity >= self.similarity_threshold:
                    if search_dict_for_term(similar_files, current_file_name):
                        self.files_to_delete.add(current_file_name)
                    else:
                        similar_files[current_file_name] = {'similar_to': similar_file_name,
                                                            'similarity': similarity}
