This application is designed to scan a directory tree containing text files to find and remove duplicate and highly similar files.

It uses an MD5 checksum approach to find and remove exact duplicate files, and a TF-IDF approach to find and remove files that exceed a user-specified similarity threshold. 

It was written in and must be run with **Python 3.5.1**. 

# Cloning this project

The first thing to do is clone this project to your local machine. To do so simply run `git clone git@gitlab.com:brandonwolfgang/c6d5e3a6e1dbb71c9dc5fe7f9fd052bf.git`, then `cd c6d5e3a6e1dbb71c9dc5fe7f9fd052bf` to navigate into the project directory.

# Installing dependencies

There are several package dependencies needed to run this application, most of which are dependencies of the [scikit-learn](http://scikit-learn.org/stable/index.html) machine learning library.

To install these dependencies, simply run `pip install -r requirements.txt`. This will install any dependent packages which are not currently installed on your system (I would suggest using [virtualenvwrapper](http://virtualenvwrapper.readthedocs.io/en/latest/index.html) to create and work in a virtual environment).

# Running the application:

Before running the application, the `data.zip` file must be unzipped. This will create a `data/` directory containing the data files to be searched.

To run the application, choose a similarity threshold (between 0 and 100 percent similarity) and enter `python app.py data <similarity_threshold>` into your command prompt.

The application will run and output a count and list of any duplicate or similar files it has found and deleted.

## Sample output:

As an example running the command `python app.py data 90` will produce the following output:

```
Successfully deleted 32 duplicate files:
['data/0009654', 'data/0009655', 'data/0009727', 'data/0009741', 'data/0010012', 'data/0010015', 'data/0010225', 'data/0010514', 'data/0010517', 'data/0010633', 'data/0010744', 'data/0010917', 'data/0011151', 'data/0011490', 'data/0011495', 'data/0011746', 'data/0012143', 'data/0012237', 'data/0012545', 'data/0012553', 'data/0012889', 'data/0096903', 'data/1', 'data/10', 'data/2', 'data/3', 'data/4', 'data/5', 'data/6', 'data/7', 'data/8', 'data/9']
Successfully deleted 59 similar files:
['data/0009661', 'data/0009887', 'data/0009934', 'data/0010019', 'data/0010032', 'data/0010034', 'data/0010134', 'data/0010173', 'data/0010210', 'data/0010220', 'data/0010257', 'data/0010303', 'data/0010535', 'data/0010587', 'data/0010654', 'data/0010717', 'data/0010737', 'data/0010751', 'data/0010792', 'data/0010797', 'data/0010825', 'data/0010866', 'data/0010915', 'data/0011054', 'data/0011118', 'data/0011157', 'data/0011205', 'data/0011225', 'data/0011237', 'data/0011265', 'data/0011283', 'data/0011343', 'data/0011377', 'data/0011431', 'data/0011525', 'data/0011582', 'data/0011624', 'data/0011651', 'data/0011654', 'data/0011962', 'data/0011979', 'data/0011983', 'data/0012020', 'data/0012033', 'data/0012216', 'data/0012413', 'data/0012801', 'data/0012839', 'data/0012853', 'data/11', 'data/12', 'data/13', 'data/14', 'data/15', 'data/16', 'data/17', 'data/18', 'data/19', 'data/20']
```

As you can see files `1-10` are in the list of successfully deleted exact duplicates, and files `11-20` are in the list of successfully deleted similar files.

## Refreshing the data:

Since the application deletes data files from the `data` directory, it is necessary to "refresh" the data before rerunning if you wish to find exact duplicates again. Otherwise the application will (rightfully) output that no duplicates were found, and no similar files will be found unless the similarity threshold is lowered on subsequent runs. 

To refresh the data, enter the following chained commands into your terminal: `rm -rf data/ ; unzip data.zip ;`.

# Running the tests:

Since running the application has deleted all duplicate files as well as any similar files exceeding the chosen similarity threshold, it is necessary to "[refresh](#refreshing-the-data)" the data directory before running any tests.

Once the data has been refreshed, tests can be run by entering the following into your command prompt: `python test_(duplicate|similar)_file_finder.py ;` where `(duplicate|similar)` is replaced by the specific test suite you wish to run.

The test suites are incomplete (i.e. not all class methods are covered), but I wanted to include them to demonstrate that I understand how to create and implement unit tests in Python.

# Retrospective:

I really enjoyed solving this problem! As expected when I initially read the instructions, the majority of the time spent was less on coding and more on researching the best way to accomplish part 2. Then it was a matter of understanding how to properly implement the solution using [scikit-learn](http://scikit-learn.org/stable/index.html). I had not been previously exposed to TF-IDF but am very thankful to have found it as it is extremely useful and interesting!
