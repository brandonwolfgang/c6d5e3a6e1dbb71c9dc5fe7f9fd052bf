import os
import unittest
from duplicate_file_finder import DuplicateFileFinder, get_file_hash


class TestDuplicateFileFinder(unittest.TestCase):
    """
    Test functions defined in `duplicate_file_finder.py`
    """

    data_path = 'data'

    def test_find_and_delete_duplicates(self, data_path=data_path):
        """
        Test the find_exact_duplicates() and delete_duplicate_files() methods
        of DuplicateFileFinder. According to initial README there are 32
        exact duplicate files. Once the files are found they are deleted and
        the deletion test occurs.
        :param data_path: The data path to search for duplicate files.
        """
        duplicate_file_finder = DuplicateFileFinder(data_path)
        duplicate_file_finder.find_exact_duplicates()
        self.assertEqual(len(duplicate_file_finder.files_to_delete), 32)
        duplicate_file_finder.delete_duplicate_files()
        self.assertEqual(len(duplicate_file_finder.files_to_delete), 0)

    def test_get_file_hash(self, data_path=data_path):
        """
        Test the get_file_hash() method of DuplicateFileFinder.
        :param data_path: The data path to read files from.
        """
        test_files = {
            '0009999': '1cbf5744dc19774d152b72090a25beb9',
            '0010000': '021b3c02b4253c51d23257daa09abbd6',
            '0010101': '667a5b62ba958d3728ca59179a8b83ac',
            '0012345': 'f8f0339d438944ea83ef13467f1167dd',
            '0012900': '7b577bb491a3e34fec499bbcd473a806',
        }

        for file_name, file_hash in test_files.items():
            self.assertEqual(file_hash, get_file_hash(os.path.join(data_path, file_name)))

if __name__ == '__main__':
    unittest.main()
