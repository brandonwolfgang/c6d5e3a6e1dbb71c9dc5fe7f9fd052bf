import os
import unittest
from similar_file_finder import SimilarFileFinder, search_dict_for_term


class TestSimilarFileFinder(unittest.TestCase):
    """
    Test functions defined in `similar_file_finder.py`
    """

    def test_search_dict_for_term(self):
        """
        Test the search_dict_for_term() method of SimilarFileFinder.
        """
        test_dict = {
            'file_1': {'similar_to': 'file_2'},
            'file_2': {'similar_to': 'file_1'},
        }

        self.assertEqual(True, search_dict_for_term(test_dict, 'file_1'))
        self.assertEqual(True, search_dict_for_term(test_dict, 'file_2'))
        self.assertEqual(False, search_dict_for_term(test_dict, 'file_3'))


if __name__ == '__main__':
    unittest.main()
