import os
import sys

from duplicate_file_finder import DuplicateFileFinder
from similar_file_finder import SimilarFileFinder


def find_and_remove_duplicate_files(data_path):
    duplicate_file_finder = DuplicateFileFinder(data_path)
    duplicate_file_finder.find_exact_duplicates()
    duplicate_file_finder.delete_duplicate_files()


def find_and_remove_similar_files(data_path, similarity_threshold):
    similar_file_finder = SimilarFileFinder(data_path, similarity_threshold)
    similar_file_finder.find_similar_files()
    similar_file_finder.delete_similar_files()


def print_invalid_path_error_message(data_path):
    print('{} is not a valid directory path, please verify.'.format(data_path))
    print_usage()


def print_similarity_threshold_error_message(similarity_threshold):
    print('Invalid similarity threshold: {}\n'
          'Value must be integer or floating point number between 0 and 100'
          ' (i.e. very low to near exact similarity).'.format(similarity_threshold))
    print_usage()


def print_usage():
    print('Usage:\tpython app.py <data_path> <similarity_threshold_percentage>\n\t'
          'e.g. python app.py data 92.5')


def main():
    if len(sys.argv) > 2:
        data_path = sys.argv[1]

        # Validate user-entered data path
        if not os.path.exists(data_path) or not os.path.isdir(data_path):
            print_invalid_path_error_message(data_path)
            sys.exit()

        # Validate user-entered similarity threshold
        try:
            similarity_threshold = float(sys.argv[2])
            if not 0 < similarity_threshold < 100:
                print_similarity_threshold_error_message(similarity_threshold)
                sys.exit()
        except ValueError:
            print_similarity_threshold_error_message(sys.argv[2])
            sys.exit()

        find_and_remove_duplicate_files(data_path)
        find_and_remove_similar_files(data_path, similarity_threshold)

    else:
        print_usage()


if __name__ == '__main__':
    main()
