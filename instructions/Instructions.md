This is a take-home code challenge for Backend Engineers, Data Scientists/Engineers, and anyone else who might end up working on large-scale data/analytics problems. 

**Estimated Time: 4-6 hours**

# Similar Documents

The goal of this problem is designed to test:
1. Your understanding of computer science fundamentals
2. Your ability to work with large, unstructured datasets and
3. Your ability to design mathematically informed algorithms to solve a real-world problem.
Be aware that this problem is not easy. I spent most of a full day of work both designing it, corralling the data and working on my solution. I’d expect you to spend at least a few hours on it.  

*Disclaimer:* It might not be immediately obvious to you why we put you through a difficult task like this as a part of the interview process. Some thoughts:
1. Would we hire a carpenter who can't build tables? A chef who can't cook? An engineer who can't write code?
2. [Joel](http://joelonsoftware.com/) made us do it.
3. It's more fun for you than us asking you trivia questions for hours.

# Background Info:

In this scenario, you are an engineer working for a company that aggregates, analyzes and publishes news data. In doing so, you scrape news articles from a variety of sources on a daily basis - you can find these in a directory called `data`, included with this challenge. For this challenge, you’ll be working with a subset of the data - approximately 3,000 documents.

As you might expect, we frequently run into duplicate data. Multiple news organizations often publish AP articles that are exactly the same. De-duplicating the data is a primary concern for our team. On top of this, we also get tons of near duplicates - articles that are only different by a small margin, maybe only differing by headline. Similar problems like incomplete articles or articles with very minor, semantically empty differences (abbreviation, for instance) also show up in this dataset.

We need ways of filtering out documents that are highly similar in order to reduce the size of the data we analyze and to reduce the effects of duplicity in our metrics reporting. Our goal is to develop a system where each news article is largely unique (i.e. not a duplicate of another very similar report) so that our analytics team can filter out truly significant reports from those which are merely widely distributed. A good example is stock tickers - they are low-information, highly similar to other reports, and widely distributed, but they aren’t usually significant. Significant articles are generally high-information and highly unique.

# Part 1:

Develop a method for removing exact duplicates from your dataset. For reference, files 1-10 in the data folder are exact duplicates and should come out. When I solved this problem, I found 32 exact duplicates.

Be sure to remove the duplicate items from the dataset before moving onto the next step.

In addition, please answer the following questions (succinctly) in your email response:
- How did you go about solving this problem? *I have done past work in other programming languages with finding exact duplicates, so the MD5 checksum approach was readily apparent to me.*
- Why did you choose this method? *Based on previous experience and additional research I determined this is the best method since the MD5 algorithm always always produces the same output given a particular input.*
- Can you be certain that your method adequately removes exact copies from the dataset? *Yes. As mentioned above the output of the MD5 algorithm is deterministic, thus all duplicates will be easily identified and removed.*

# Part 2:

Develop a method for removing near duplicates from your dataset. It is up to you how similar the documents must be for us to want to get rid of them but there should be some math that defines document similarity in your method. Keep in mind that any concept of similarity is going to be rather arbitrary at the end of the day - so inspecting your results and checking that what you're doing is actually returning what you want it to is going to help.

Be warned that there are many ways of doing this and that most of them are computationally expensive - don't worry if your method needs some time to run. We care more that you do your research and write out an informed algorithm than we do about you trying to break performance records.

We hope that for whichever method you choose, you can provide a solid, mathematical argument for *why* you did so. Files `11-20` in the `data` directory are manually edited copies of other files - they may or may not show up depending on your method but it’s a good sign if they do. For reference: my solution found between 50 & 100 highly similar items, depending on how picky I was about similarity.

In addition, please answer the following questions (succinctly):
- How did you go about solving this problem?
*The first step was to do some preliminary research on how to identify similarity between text files. In this process I discovered [TF-IDF](http://www.tfidf.com/) (term frequency-inverse document frequency), which is a standard weighting formula used in information retrieval and text mining. Further research proved that the [scikit-learn](http://scikit-learn.org/stable/index.html) machine learning library has a [TF-IDF Vectorizer](http://scikit-learn.org/stable/modules/generated/sklearn.feature_extraction.text.TfidfVectorizer.html) object whose [fit_transform()](http://scikit-learn.org/stable/modules/generated/sklearn.feature_extraction.text.TfidfVectorizer.html#sklearn.feature_extraction.text.TfidfVectorizer.fit_transform) method returns a TF-IDF weighted document-term matrix. Since each element of this matrix represents the cosine similarity between two documents, the indices of any given element can then be used to determine which files are similar to one another.*
- Why did you choose this method? *There are several reasons I chose the TF-IDF approach, but the main one is that TF-IDF is the standard when it comes to determining similarity between text documents.*
- Inspect your results and briefly comment on the types of results you see. Are there edge cases that the engineering team should be aware of? *I've inspected the results using a diffing application and providing various similarity thresholds and never once produced a false positive. There were initially edge cases when the similarity between two files was between 0.99999 and 1, until I realized that I was comparing the exact same files. As it stands now I don't see any edge cases.*

# When You're Done:

Hopefully you've had a wonderful time solving this problem.
