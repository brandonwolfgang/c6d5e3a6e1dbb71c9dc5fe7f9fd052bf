import hashlib
import os


def get_file_hash(file_path):
    """
    Helper function to get the md5 hash of a particular file.
    :param file_path: The file to hash.
    :return: The md5 hash for the file passed in.
    """
    with open(file_path, 'rb') as file_to_hash:
        file_data = file_to_hash.read()
        file_hash = hashlib.md5(file_data).hexdigest()

    return file_hash


class DuplicateFileFinder:
    """
    Finds duplicate files in directory tree by comparing md5 hash values of files.
    """

    def __init__(self, data_path):
        """
        Instantiates a DuplicateFileFinder object.
        :param data_path: The data path to search for duplicate files.
        """
        self.data_path = data_path
        self.files_to_delete = []

    def delete_duplicate_files(self):
        """
        Deletes duplicate files by iterating over the files_to_delete list.
        """
        if len(self.files_to_delete) > 0:
            for file in self.files_to_delete:
                os.remove(file)
            print('Successfully deleted {} duplicate files:\n{}'.format(len(self.files_to_delete),
                                                                        [file for file in self.files_to_delete]))
            self.files_to_delete = []
        else:
            print('No duplicate files found.')

    def find_exact_duplicates(self):
        """
        Finds duplicate files by walking directory tree and calculating md5 hash for
        each file encountered. If any hash is found in the hash_set (i.e. the exact
        same file has been encountered previously), the current file name is appended
        to the files_to_delete list.
        """
        hash_set = set()

        for directory_name, subdirectories, file_list in os.walk(self.data_path):
            for file_name in file_list:
                file_path = os.path.join(directory_name, file_name)
                file_hash = get_file_hash(file_path)

                if file_hash in hash_set:
                    self.files_to_delete.append(file_path)
                else:
                    hash_set.add(file_hash)
